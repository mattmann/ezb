/**
 * \file ezb.h
 *
 * \brief Funktionen und Definitionen zur globalen Verwendung
 *
 * Diese Datei beinhaltet alle Deklarationen von Variablen und Funktione die global verwendet
 * werden.
 *
 */


#ifndef EZB_H_
#define EZB_H_

/* Importierte Dateien */
#include "Arduino.h"
#include <Stdint.h>

#define RAM_AVAIL 2048 //!< Auf dem Arduino Uno verfügbarer RAM
#define BUFFER_DEPTH 5 //!< Anzahl Zeilen auf Task-Info
#define STRING_LENGTH 80 //!< Anzahl Zeichen im String
#define INFO_STRING_LENGTH 32 //!< Anzahl der Zeichen pro Info-Zeile
#define TASKS_NUM 5 //!< maximale Anzahl an Tasks (Es ist zu beachten, dass ein nullter Task ohne Funktion exisitert.)
#define STACK_SIZE 180 //!< Größe des Stacks jedes Tasks (Nach derzeitiger Konfiguration 160 mindestens nötig)
#define INT_TIME 10000 //!< Zeit bis zum Interrupt (Nicht weniger als 10000, da sonst kein Systemstart vor dem ersten Interrupt möglich ist)
#define TIME_SLICE 3 //!< Erlaubte CPU-Ticks bei Round Robin

extern volatile uint32_t time_now;
extern uint8_t tasks_created;
extern volatile uint8_t rdy_count;
extern volatile uint8_t run;
extern volatile uint8_t run_next;
extern volatile uint8_t rdytsk[TASKS_NUM];
extern volatile char str[STRING_LENGTH];
extern volatile char* name_string;
extern volatile uint8_t taskToMonitor;
extern unsigned char __heap_start; //!< Systemvariable zum Startwert des Heaps
extern unsigned char __data_end; //!< Systemvariable zum Ende des Data-Blocks

/** \brief Return Code
 *
 * Diese Struktur gibt zwei verschiedene Return Codes vor. NO_ERROR
 * bedeutet ein fehlerloses Ablaufen. ERROR sagt aus, dass ein Fehler
 * vorliegt.
 */
enum RETURN_CODE{
  NO_ERROR, //!< Kein Error
  ERROR //!< Error
};


/** \brief Zustand
 *
 * Diese Struktur gibt vier verschiedene Zustände vor. Ein Task kann
 * nur einen dieser Zustände annehmen. Eine Beschreibung der einzelnen
 * Zustände findet sich im Skript
 */
enum ZUSTAND{
  DORMANT=0, //!< Ruhend
  READY=1, //!< Bereit
  RUNNING=2, //!< Laufend
  BLOCKED=3 //!< Blockiert
};

/** \brief Taskverwaltungsblock
 *
 * Diese Struktur stellt ein Element eines Taskverwaltungsblocks dar.
 *
 */
struct TVB{
  volatile unsigned stack_pointer; //!< Zeigt auf die aktuelle Position im Stack
  unsigned stack_top; //!< Zeigt auf den obersten Eintrag des Stacks
  volatile uint8_t time_p; //!< Zykluszeit/Periode
  volatile uint8_t time_w; //!< Wartezeit
  volatile boolean slice_reached; //!< Flagge für RoundRobin Scheduler
  volatile uint32_t time_r; //!< Gelaufene Zeit
  uint8_t prio; //!< Priorität
  volatile enum ZUSTAND state; //!< Zustand
  void (*function)(void); //!< Funktion, bzw. Titel/Aufgabe
  volatile boolean noend; //!< Gibt an ob eine Funktion den Prozessor freigeben darf oder nicht
};

/** \brief Stackpointer
 *
 * Diese Struktur stellt einen Stackpointer dar.
 */
struct Stackpointer{
	volatile unsigned stack_pointer; //!< Stackpointer
};

extern unsigned SPA;
extern unsigned SPB;
extern struct TVB TVB_use1[TASKS_NUM];
extern struct TVB *TVB_use;
extern struct Stackpointer *CurrentTVB;
extern struct Stackpointer *NewTVB;


#ifdef __cplusplus
extern "C"{
#endif
	
/** \brief Stack Setup
 *
 * Diese Funktion bereitet den Stack für alle verwendten Tasks vor. Dies
 * geschieht vor dem ersten Context-Switch und ist elementar wichtig, damit
 * alles reibungslos funktioniert.
 * Als erstes wird die exit-Adresse auf den Stack gelegt, darüber die Adresse
 * der aufzurufenden Funktion und anschließend die Register. Durch diese
 * Reihenfolge wird die korrekte Ausführung garantiert.
 * \param starting_point [in] Pointer auf die aufzurufende Funktion
 * \param stack_top [in] Top of Stack des Tasks
 * \param EZB_ExitTask [in] Pointer auf Exit Funktion
 * \return new_sp
 */
unsigned STACK_SETUP(void (*starting_point)(void), unsigned stack_top, void (*EZB_ExitTask)(void));

/** \brief Initialisierungsfunktion der Zeitverwaltung
 *
 * Diese Funktion ist verantwortlich für das aktivieren des aktuell
 * geforderten Timers. Dabei besteht die Auswahl zwischen dem WDT und TIMER1.
 * TIMER1 nimmt als Argument den gewünschten Zeitvergleichswert, während
 * der WDT von Hand auf die gewünschte Zeit konfiguriert werden muss. Dies
 * erfolgt in der Datei Makros.h
 * \param int_time [in] Zeit bis zum Interrupt
 * \return None
 */
void EZB_Zeitverwaltung_Init(uint32_t int_time);

/** \brief Initialisierungsfunktion des WDT
 *
 * Diese Funktion kann verwendet werden um die Verwendung des
 * Watchdog-Timers einzurichten. Dieser kann Resets oder Interrupts auslösen
 * und ist ein Hardware-Timer der, im Gegensatz zu den anderen Timern, keine
 * Pins belegt. Allerdings müssen nach dem Setzen der Aktivieriungsbits
 * innerhalb von 4 Takten die Einstellungen vorgenommen werden. Da dies nur
 * unter Verwendung von Optimierungsoptionen beim Kompilieren funktioniert
 * wurde diese Funktion in Inline-Assembler neu geschrieben und findet sich
 * in der Datei Makros.h. Dieser Code dient nur der Information und der
 * Darstellung einer Umsetzung in C.
 * \return None
 */
void wdt_setup(void);

/** \brief Initialisierungsfunktion des TIMER1
 *
 * Diese Funktion wird verwendet um einen Hardware-Timer (TIMER1) zu
 * aktivieren. Dieser Timer ist ein 16bit Timer. Dieser Timer blockiert die
 * Pulsweitenmodulation (PWM) an Pin Nummer 9 und Nummer 10. Für diesen Timer
 * wird der Clear Timer on Compare Match (CTC) Mode verwendet. Das bedeutet,
 * dass er nach erreichen eines Vergleichswerts, hier "microseconds" auf 0
 * zurückgesetzt wird und einen Interrupt auslöst.
 * 
 * WGM13 und WGM12 müssen auf 1 gesetzt sein um den CTC Mode zu aktivieren.
 * Die Einstellung des Prescalers wird anschließend über die Bits CS10, CS11
 * und CS12 gesetzt.
 * 
 * \param microseconds [in] Wert bis zum Interrupt in Mikrosekunden
 * \return None
 */
void timer1_setup(uint32_t microseconds);

/** \brief Initialisierungsfunktion des EZB
 *
 * Diese initialisiert das EZB. Sie setzt die Startwerte jedes Tasks und
 * die Ausgabefunktion und den Interrupt-Timer
 * \return None
 */
void EZB_Init(void);

/** \brief Aktivierung eines Tasks
 *
 * Diese Funktion aktiviert einen Task.
 * \param i [in] Nummer des Tasks
 * \return None
 */
void EZB_Aktivierung(uint8_t i);

/** \brief Deaktivierung eines Tasks
 *
 * Diese Funktion deaktiviert einen Task und setzt ihn auf Anfangswerte zurück.
 * \param i [in] Nummer des Tasks
 * \return None
 */
void EZB_Deaktivierung(uint8_t i);

/** \brief Laufendsetzen eines Tasks
 *
 * Diese Funktion setzt den Status eines Tasks auf LAUFEND.
 * \param i [in] Nummer des Tasks
 * \return None
 */
void EZB_Laufend(uint8_t i);

/** \brief Blockierung eines Tasks
 *
 * Diese Funktion blockiert einen Task.
 * \param i [in] Nummer des Tasks
 * \return None
 */
void EZB_Blockiert(uint8_t i);

/** \brief Task-Suche
 *
 * Diese Funktion sucht nach dem nächsten bereiten Task. Sie vergleicht dabei die Prioritäten der
 * bereiten Tasks mit der des laufenden und legt so den als nächstes laufenden Task fest.
 * \return None
 */
void EZB_Suche(void);

/** \brief Zykluszeitbildung
 *
 * Diese Funktion bildet die Zykluszeit des Schedulers
 * \return None
 */
void EZB_Zeit(void);

/** \brief Exitfunktion
 *
 * Diese Funktion wird aufgerufen falls fälschlicherweise ein Task returned. Sie setzt ihn
 * zurück und verweist auf die zweite Exitfunktion.
 * \return None
 */
void EZB_ExitTask(void);

/** \brief Exitfunktion 2
 *
 * Diese Funktion wird aufgerufen falls fälschlicherweise ein Task returned. Sie setzt ihn
 * zurück und verweist auf die erste Exitfunktion.
 * \return None
 */
void EZB_ExitTask2(void);

/** \brief Innere Abläufe der Freigabe Funkion
 *
 * Diese Funktion entscheidet ob bei Prozessorfreigabe ein andere Task laufen kann
 * \return None
 */
void EZB_Wait_Internals(void);

/** \brief Innere Abläufe der Task Start Funkion
 *
 * Diese Funktion entscheidet ob bei Prozessorfreigabe ein andere Task laufen kann
 * \return None
 */
void EZB_StartTask_Internals(void);

/** \brief Freigabe Funkion
 *
 * Diese Funktion wird aufgerufen um nach dem Erledigen eines Tasks den Prozessor freizugeben
 * \return None
 */
void EZB_Wait(void)__attribute__((naked));

/** \brief Task Start Funkion
 *
 * Diese Funktion wird aufgerufen um ausgehend vom Setup einen Task zu starten
 * \return None
 */
void EZB_StartTask(void)__attribute__((naked));

/** \brief System Start Funktion
 *
 * Diese Funktion startet das EZB.
 * \return None
 */
void EZB_SystemStart(void);

/** \brief Erstellung eines Tasks
 *
 * Diese Funktion erstellt einen neuen Task. Dabei wird die Anzahl der erstellen Tasks
 * inkrementiert.
 * \param func [in] Pointer auf Task-Funktion
 * \param cycle [in] Periode des Tasks
 * \param priority [in] Priorität des Tasks
 * \return NO_ERROR
 */
enum RETURN_CODE EZB_CreateTask(void (*func)(void), uint8_t cycle, uint8_t priority);

/** \brief Funktion zur graphischen Ausgabe von Variablen
 *
 * Diese Funktion gibt Variablen an das Terminal weiter.
 * \param x [in] Horizontale Position der Ausgabe
 * \param y [in] Vertikale Position der Ausgabe 
 * \return None
 */
static void EZB_Info_Line(uint8_t x, uint8_t y);

/** \brief Funktion zu graphischen Ausgabe von Text
 *
 * Diese Funktion gibt Text an das Terminal weiter.
 * \param x [in] Horizontale Position der Ausgabe
 * \param y [in] Vertikale Position der Ausgabe 
 * \param string [in] Pointer auf den auszugebenden String
 * \return None
 */
static void EZB_Info_Line_PROGMEM(uint8_t x, uint8_t y, char* string);
	
/** \brief Initialisierungsfunktion der Ausgabe
 *
 * Diese Funktion initialisiert die graphische Ausgabe des EZB. Sie legt das Gerüst an und die Farben fest.
 * \return None
 */
void EZB_Info_Init(void);

/** \brief Info Ausgabe Funktion
 *
 * Diese Funktion schreibt eine Ausgabe in den Puffer
 * \param string [in] Text für die Ausgabe
 * \return None
 */
void EZB_Info_Write(const char * string);

/** \brief Alternative Info Ausgabe Funktion
 *
 * Diese Funktion schreibt einen String in den Puffer
 * \param string [in] Text für die Ausgabe
 * \return None
 */
void EZB_Info_Write_PROGMEM(char* string);

/** \brief EZB Info Update Funktion
 *
 * Diese Funktion gibt den Puffer auf den Info-Monitor aus.
 * \return None
 */
void EZB_Info_Update(void);

/** \brief Alternative EZB Info Update Funktion
 *
 * Diese Funktion gibt den Puffer auf den Info-Monitor aus.
 * Diese Funktion ist zu verwenden mit EZB_Info_Write_PROGMEM.
 * \return None
 */
void EZB_Info_Update_PROGMEM(void);

/** \brief EZB Prozessor Update Funktion
 *
 * Diese Funktion gibt den Prozessorzustand aus.
 * \return None
 */
void EZB_Info_Processor(void);

/** \brief EZB Daten Info Funktion
 *
 * Diese Funktion gibt die Größe des mit globalen Variablen belegten Speichers aus.
 * Das ist wichtig, da es geschehen kann, dass der Stack in diesen Variablen-Speicher
 * hinein wächst.
 * \return None
 */
void EZB_Info_Data(void);

/** \brief Task Update Funktion
 *
 * Diese Funktion updatet den Status, die Wartezeit und Periode eines Tasks.
 * \param taskId [in] Nummer des Tasks
 * \return None
 */
void EZB_Info_Update_Task(uint8_t taskId);

/** \brief Alles Tasks Update Funktion
 *
 * Diese Funktion updatet alle Tasks.
 * \return None
 */
void EZB_Info_Update_All_Tasks(void);

/** \brief Stack Update Funktion
 *
 * Diese Funktion updatet den Stacktop, Stackpointer und den benutzten Stack eines Tasks.
 * \param taskId [in] Nummer des Tasks
 * \return None
 */
void EZB_Info_Update_Stack(uint8_t taskId);

/** \brief Alle Stacks Update Funktion
 *
 * Diese Funktion updatet die Stackinformationen aller Tasks.
 * \return None
 */
void EZB_Info_Update_All_Stack(void);

/** \brief Task Listen Funktion
 * 
 * Disee Funktion schreibt eine Liste der bereiten Tasks geordnet nach ihrer Priorität.
 * \return None
 */
void EZB_Info_Update_List(void);

/** \brief Initialisierungsfunktion des Taskmonitors
 *
 * Diese Funktion initialisiert den Taskmonitor des EZB.
 * \return None
 */
void EZB_Info_Task_Monitor_Init(uint8_t i);

/** \brief Taskmonitor Schrittfunktion
 *
 * Diese Funktion macht Schritte auf dem Taskmonitor. Sie stellt damit
 * den Status des derzeit ausgewählten Tasks dar.
 * \param i [in] Nummer des ausgewählten Tasks
 * \return None
 */
void EZB_Info_Task_Monitor_Step(uint8_t i);

/** \brief Display Update
 *
 * Diese Funktion aktualisiert alle Bereiche der graphischen Ausgabe.
 * \return None
 */
void EZB_Info(void);

#ifdef __cplusplus
}
#endif

#endif /* EZB_H */

/* EOF */