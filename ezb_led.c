/**
 * \file ezb_led.c
 *
 * \brief Funktionen und Definitionen zur Verwendung von LEDs
 *
 * Diese Datei beinhaltet alle Defitionen und Funktionen die zur Ansteuerung der LEDs verwendet
 * werden.
 *
 */

/** \addtogroup LEDs
 * \brief Verwaltung der On-Board LEDs
 */
/*@{*/

/* Importiere Dateien */
#include <Arduino.h>
#include <avr/io.h>
#include <Stdint.h>
#include "ezb_led.h"

/*****************************************************************************/
#define LEDS_LED1        (1 << 0) //!< LED Maske für die erste LED auf dem Board.
#define LEDS_LED2        (1 << 1) //!< LED Maske für die zweite LED auf dem Board.
#define LEDS_ALL_LEDS    (LEDS_LED1 | LEDS_LED2) //!< LED Maske für alle LEDs auf dem Board.
#define LEDS_NO_LEDS     0 //!< LED Maske für keine LED auf dem Board.

/*****************************************************************************/
void EZB_LED_Init(void)
{
DDRD  |= LEDS_LED1;
PORTD |= LEDS_LED1;	
}
/*****************************************************************************/
void EZB_LED_TurnOn(void)
{
PORTD |= LEDS_LED1;
}
/*****************************************************************************/
void EZB_LED_TurnOff(void)
{
PORTD &= ~LEDS_LED1;
}
/*****************************************************************************/
void EZB_LED_Toggle(void)
{
static led;
PORTD &= ~LEDS_LED1;
}

/*@}*/
/* EOF */