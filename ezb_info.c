/**
 * \file ezb_info.c
 *
 * \brief Funktionen und Variablen zur graphischen Darstellung
 *
 * Diese Datei beinhaltet alle Variabeln und Funktionen die zur graphischen Ausgabe verwendet
 * werden.
 *
 */

/** \addtogroup GUI
 */
/*@{*/

/* Importiere Dateien */
#include "ezb.h"
#include "ezb_out.h"
#include "ezb_uart.h"
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <avr/io.h>
#include <stdio.h>

/* Definitionen */
#define START 3 //!< Startzeile
#define START_NEXT START+3+TASKS_NUM-1 //!< Startzeile für zweiten Block
#define START_NEXT_PROC_X 3 //!< Startzeile für Prozessorinfo
#define START_NEXT_PROC_Y 40 //!< Startspalte für Prozessorinfo

#define START_DATA_INFO START_NEXT_PROC_Y+26 //!< Startspalte für die Data Info

#define START_NEXT_TASK_Y 28 //!< Startspalte für Task Liste

#define START_TASK_MONI 18 //!< Startzeile für Task-Info
#define START_TASK_MONI_Y 1 //!< Startspalte für Task-Info


/* Variablen */
volatile uint8_t last_write; //!< Task der zuletzt geschrieben hat
volatile uint8_t info_line; //!< Aktuell zu beschreibende Zeile
volatile uint8_t step; //!< Schritt des Taskmonitors
volatile char str[STRING_LENGTH]; //!< String für alle Ein- und Ausgaben
volatile char info_str[INFO_STRING_LENGTH]; //!< String für die Ausgabe auf die Info-Zeile
volatile char* name_string; //!< String für die Ausgabe der Tasknamen
volatile uint8_t taskToMonitor=1; //!< Im Task-Monitor dargestellter Task

/* Strings für die Ausgabe */
#ifndef DOXYGEN
prog_char string_top[] ="                     ILS    EZB-LUFTFAHRTSYSTEM I                          V1   "; //!< Kopfzeile der GUI

prog_char string_prozessor_info_1[] ="    PROZESSOR INFO    "; //!< Kopfzeile der Prozessorinfo
prog_char string_prozessor_info_2[] ="REG   Val | REG    Val"; //!< Zeile 1 der Prozessorinfo
prog_char string_prozessor_info_3[] ="WD        | SREG      "; //!< Zeile 2 der Prozessorinfo
prog_char string_prozessor_info_4[] ="MCUSR     |           "; //!< Zeile 3 der Prozessorinfo
prog_char string_prozessor_info_5[] ="UBRR0H    | UBRR0L    "; //!< Zeile 4 der Prozessorinfo
prog_char string_prozessor_info_6[] ="PORTD     | DDRD      "; //!< Zeile 5 der Prozessorinfo

prog_char string_data_info_1[] =" DATA INFO "; //!< Kopfzeile der Data Info
prog_char string_data_info_2[] =".data:     "; //!< .data Ausgabezeile
prog_char string_data_info_3[] =" .bss:     "; //!< .bss Ausgabezeile
prog_char string_data_info_4[] =" Free:     "; //!< Ausgabezeile des freien Speichers
prog_char string_data_info_5[] =" Used:     "; //!< Ausgabzeile des verwendeten Speichers

prog_char string_task_info_1[] ="     TASKS INFO     "; //!< Kopfzeile der Task Info
prog_char string_task_info_2[] =" Nr.   State  z    a"; //!< Zeile 1 der Task Info

prog_char string_task_list_1[] ="TASK"; //!< Kopfzeile 1 der ABWS Info
prog_char string_task_list_2[] ="LIST"; //!< Kopfzeile 2 der ABWS Info

prog_char string_info_1[] ="INFO                                 "; //!< Kopfzeile der Info
prog_char string_stack_setup_1[] ="STACK-SETUP          Current SP:     "; //!< Kopfzeile der Stack Info
prog_char string_stack_setup_2[] =" Nr.    Top    Pointer      Used    %"; //!< Zeile 1 der Stack Info

prog_char string_bottom[] ="                     ILS    EZB-LUFTFAHRTSYSTEM I                         V1    "; //!< Fußzeile der GUI

prog_char string_task_monitor_1[] ="TASK MONITOR FOR TASK "; //!< Kopzeile des Task Monitors
prog_char string_task_monitor_2[] ="B"; //!< Zeile Blockiert
prog_char string_task_monitor_3[] ="L"; //!< Zeile Laufend
prog_char string_task_monitor_4[] ="B"; //!< Zeile Bereit
prog_char string_task_monitor_5[] ="R"; //!< Zeile Ruhend
prog_char string_task_monitor_6[] =" "; //!< Leerzeichen

prog_char step_char[] ="-"; //!< Schrittzeichen
#endif
					
/*****************************************************************************/
static void EZB_Info_Line(uint8_t x, uint8_t y)
{
	gotoxy(x,y);
	write_to(str);
}
/*****************************************************************************/
static void EZB_Info_Line_PROGMEM(uint8_t x, uint8_t y, char* string)
{
	gotoxy(x,y);
	write_from_id(string);
}
/*****************************************************************************/
void EZB_Info_Init(void){
  SetColor(VORDER_GRUND_WEISS,HINTER_GRUND_SCHWARZ);
  blackscr();
  SetColor(VORDER_GRUND_WEISS,HINTER_GRUND_BLAU);
  writeTop (string_top);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X,START_NEXT_PROC_Y,string_prozessor_info_1);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+1,START_NEXT_PROC_Y,string_prozessor_info_2);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+2,START_NEXT_PROC_Y,string_prozessor_info_3);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+3,START_NEXT_PROC_Y,string_prozessor_info_4);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+4,START_NEXT_PROC_Y,string_prozessor_info_5);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+5,START_NEXT_PROC_Y,string_prozessor_info_6);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X,START_DATA_INFO,string_data_info_1);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+1,START_DATA_INFO,string_data_info_2);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+2,START_DATA_INFO,string_data_info_3);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+3,START_DATA_INFO,string_data_info_4);
  EZB_Info_Line_PROGMEM(START_NEXT_PROC_X+4,START_DATA_INFO,string_data_info_5);
  EZB_Info_Line_PROGMEM(START,0,string_task_info_1);
  EZB_Info_Line_PROGMEM(START+1,0,string_task_info_2);
  EZB_Info_Line_PROGMEM(START,START_NEXT_TASK_Y,string_task_list_1);
  EZB_Info_Line_PROGMEM(START+1,START_NEXT_TASK_Y,string_task_list_2);
  EZB_Info_Line_PROGMEM(START_NEXT,0,string_info_1);
  EZB_Info_Line_PROGMEM(START_NEXT,START_NEXT_PROC_Y,string_stack_setup_1);
  EZB_Info_Line_PROGMEM(START_NEXT+1,START_NEXT_PROC_Y,string_stack_setup_2);
  writeBottom(string_bottom);
  SetColor(VORDER_GRUND_WEISS,HINTER_GRUND_SCHWARZ);
  EZB_Info_Task_Monitor_Init(taskToMonitor);
}
/*****************************************************************************/
void EZB_Info_Task_Monitor_Init(uint8_t i)
{
	EZB_Info_Line_PROGMEM(START_TASK_MONI,START_TASK_MONI_Y,string_task_monitor_1);
	snprintf(str, 2,"%d",i);
	EZB_Info_Line(START_TASK_MONI,START_TASK_MONI_Y+22);
	clrrow(START_TASK_MONI+4);
	clrrow(START_TASK_MONI+3);
	clrrow(START_TASK_MONI+2);
	clrrow(START_TASK_MONI+1);
	EZB_Info_Line_PROGMEM(START_TASK_MONI+1,START_TASK_MONI_Y,string_task_monitor_2);
	EZB_Info_Line_PROGMEM(START_TASK_MONI+2,START_TASK_MONI_Y,string_task_monitor_3);
	EZB_Info_Line_PROGMEM(START_TASK_MONI+3,START_TASK_MONI_Y,string_task_monitor_4);
	EZB_Info_Line_PROGMEM(START_TASK_MONI+4,START_TASK_MONI_Y,string_task_monitor_5);
	step=1;
}
/*****************************************************************************/
void EZB_Info_Task_Monitor_Step(uint8_t i)
{
	char c;
	EZB_Info_Line_PROGMEM(START_TASK_MONI+4-TVB_use[i].state ,START_TASK_MONI_Y+step,step_char);
	step++;
	if(step == MAX_WINDOW_WIDTH) {
		step =1;
		c = EZB_uart_getchar();
		if ( c != 0){
			taskToMonitor++;
			if (taskToMonitor > tasks_created) taskToMonitor=1;
			//EZB_Info_Init();
		}
		EZB_Info_Task_Monitor_Init(taskToMonitor);
	}
}
/*****************************************************************************/
void EZB_Info_Update_Task(uint8_t taskId)
{
	if(taskId!=0){
    snprintf(str, STRING_LENGTH,"%5d%5d%5d%5d",taskId,TVB_use[taskId].state,TVB_use[taskId].time_w,TVB_use[taskId].time_p);
    EZB_Info_Line(START+1+taskId,0);
	if(taskId == taskToMonitor) EZB_Info_Task_Monitor_Step(taskToMonitor);
}
}
/*****************************************************************************/
void EZB_Info_Update_All_Tasks(void){
	uint8_t i;
	for(i=1; i<=tasks_created; i++){
		EZB_Info_Update_Task(i);
	}
}
/*****************************************************************************/
void EZB_Info_Update_Stack(uint8_t taskId)
{
	unsigned sp = (SPH << 8) | SPL; //auslesen des aktuellen Stackpointers
	    snprintf(str, STRING_LENGTH,"%5d", sp);
	    EZB_Info_Line(START_NEXT,START_NEXT_PROC_Y+32);
	uint8_t used_stack=TVB_use[taskId].stack_top-TVB_use[taskId].stack_pointer;
	int size = snprintf(str, STRING_LENGTH,"%3d%8d%11d%6d/%3d  ",taskId,TVB_use[taskId].stack_top, TVB_use[taskId].stack_pointer,used_stack,STACK_SIZE);
	snprintf(str+size, STRING_LENGTH-size, "%3d", 100*used_stack/STACK_SIZE);
	EZB_Info_Line(START_NEXT+1+taskId,START_NEXT_PROC_Y);
}
/*****************************************************************************/
void EZB_Info_Update_All_Stack(void){
	uint8_t i;
	for(i=1; i<=tasks_created; i++){
		EZB_Info_Update_Stack(i);
	}
}
/*****************************************************************************/
void EZB_Info_Update_List(void)
{
	uint8_t i;
	for(i=0; i<rdy_count; i++){
		snprintf(str, STRING_LENGTH," %2d ", rdytsk[i]);
		EZB_Info_Line(START+2+i,START_NEXT_TASK_Y);
	}
	for(i=rdy_count; i<tasks_created; i++){
		snprintf(str, STRING_LENGTH, "    ");
		EZB_Info_Line(START+2+i, START_NEXT_TASK_Y);
	}
}
/*****************************************************************************/
void EZB_Info_Write(const char * string)
{
	strncpy(info_str,string,INFO_STRING_LENGTH);
}
/*****************************************************************************/
void EZB_Info_Write_PROGMEM(char* string)
{
  name_string=string;
}
/*****************************************************************************/
void EZB_Info_Update(void)
{
    if(last_write!=run){
		char time_str[30];
	    int size=snprintf(time_str, STRING_LENGTH,"%ld: ",time_now);
	    gotoxy(START_NEXT+1+info_line,0);
		write_to(time_str);
		gotoxy(START_NEXT+1+info_line,size+1);
		write_to(info_str);
		info_line++;
		if(info_line==BUFFER_DEPTH){
			info_line=0;
		}
		last_write=run;
	}
}
/*****************************************************************************/
void EZB_Info_Update_PROGMEM(void)
{
    if(last_write!=run){
		char time_str[30];
	    int size=snprintf(time_str, STRING_LENGTH,"%ld: ",time_now);
	    gotoxy(START_NEXT+1+info_line,0);
		write_to(time_str);
		EZB_Info_Line_PROGMEM(START_NEXT+1+info_line,size+1,name_string);
		info_line++;
		if(info_line==BUFFER_DEPTH){
			info_line=0;
		}
		last_write=run;
	}
}
/*****************************************************************************/
void EZB_Info_Processor(void)
{
		snprintf(str, 4,"%3x",WDTCSR);
		EZB_Info_Line(START_NEXT_PROC_X+2,START_NEXT_PROC_Y+7);
		snprintf(str, 4,"%3x",SREG);
		EZB_Info_Line(START_NEXT_PROC_X+2,START_NEXT_PROC_Y+19);
		snprintf(str, 4,"%3x",MCUSR);
		EZB_Info_Line(START_NEXT_PROC_X+3,START_NEXT_PROC_Y+7);
		snprintf(str, 4,"%3x",UBRR0H);
		EZB_Info_Line(START_NEXT_PROC_X+4,START_NEXT_PROC_Y+7);
		snprintf(str, 4,"%3x",UBRR0L);
		EZB_Info_Line(START_NEXT_PROC_X+4,START_NEXT_PROC_Y+19);
		snprintf(str, 4,"%3x",PORTD);
		EZB_Info_Line(START_NEXT_PROC_X+5,START_NEXT_PROC_Y+7);
		snprintf(str, 4,"%3x",DDRD);
		EZB_Info_Line(START_NEXT_PROC_X+5,START_NEXT_PROC_Y+19);
		
}
/*****************************************************************************/
void EZB_Info_Data(void)
{
	uint16_t data_hp=&__heap_start;
	uint16_t data_sp=TVB_use[tasks_created].stack_pointer;
	uint16_t data_data=&__data_end-256;
	uint16_t data_bss=data_hp-data_data;
	uint16_t FreeStack=data_sp-data_hp;
	uint16_t UsedStack=2048-FreeStack;
	snprintf(str, 6,"%4d",data_data);
	EZB_Info_Line(START_NEXT_PROC_X+1,START_DATA_INFO+7);
	snprintf(str, 6,"%4d",data_bss);
	EZB_Info_Line(START_NEXT_PROC_X+2,START_DATA_INFO+7);
	snprintf(str, 6,"%4d",FreeStack);
	EZB_Info_Line(START_NEXT_PROC_X+3,START_DATA_INFO+7);
	snprintf(str, 6,"%4d",UsedStack);
	EZB_Info_Line(START_NEXT_PROC_X+4,START_DATA_INFO+7);
}
/*****************************************************************************/
void EZB_Info(void)
{
	Send_Alive_Signal();
	EZB_Info_Update_All_Tasks();
	EZB_Info_Update_All_Stack();
	EZB_Info_Update_List();
  	EZB_Info_Processor();
	EZB_Info_Data();
}
/*@}*/
/* EOF */

