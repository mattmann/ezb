/**
 * \file ezb_zeitverwaltung.c
 *
 * \brief Funktionen zur Zeitverwaltung
 *
 * Diese Datei beinhaltet alle Funktionen die direkt mit der
 * Zeitverwaltung zu tun haben.
 *
 */

/** \addtogroup Zeit
 * \brief Die Funktionen dieser Gruppe sind Teil der Zeitverwaltung.
 */
/*@{*/

/* Importiere Dateien */
#include "ezb.h"
#include "Makros.h"
#include "avr/io.h"

/*****************************************************************************/
//#define USE_WDT //!< Verwendung des Watchdog-Timers

/*****************************************************************************/
void EZB_Zeit(void){
	/* Zeitzähler inkrementieren */
	time_now++;
	EZB_Blockiert(run);
	OLD_SP;
	/* Für alle Tasks Wartezeit dekrementieren */
	uint8_t i;
	for(i=1; i<=tasks_created; i++){
		TVB_use[i].time_w=TVB_use[i].time_w-1;
		/* Wenn Wartezeit abgelaufen, zurücksetzen */
		if(TVB_use[i].time_w==0){
			TVB_use[i].time_w=TVB_use[i].time_p;
			EZB_Aktivierung(i);
		}
		/* Laufzeit des laufenden Tasks inkrementieren */
		if(run==i){
			TVB_use[i].time_r=TVB_use[i].time_r+1;
			/* Bei Round Robin überprüfen ob Time-Slice erreicht */
			if(TVB_use[i].time_r==TIME_SLICE){
				TVB_use[i].slice_reached=1;
			}
		}
	}
	EZB_Suche();
	EZB_Laufend(run_next);
	NEW_SP;
}
/*****************************************************************************/
void wdt_setup(void){
	cli();
	__asm__ volatile ("wdr"); //Reset der Timer-Flags
	
	// WDTCSR Konfiguration:
	// Das WDTCSR Register besteht aus den folgenden Bits:
	// WDIF, WDIE, WDP3, WDCE, WDE, WDP2, WDP1, WDP0
	//
	// Diese können folgendermaßen gesetzt werden:

	// WDIE = 1 Interrupt Enable
	// WDE = 0 Reset Disable

	// Die 4 WDP-Bits und ihre Werte um gewisse Zeit in Millisekunden zu erzielen
	// WDP3	WDP2	WDP1	WDP0	Zeit bis Interrupt
	// 0		0		0		0		16
	// 0		0		0		1		32
	// 0		0		1		0		64
	// 0		0		1		1		125
	// 0		1		0		0		250
	// 0		1		0		1		500
	// 0		1		1		0		1000
	// 0		1		1		1		2000
	// 1		0		0		0		4000
	// 1		0		0		1		8000
	//
	// Um den Watchdog Setup-Modus zu aktivieren müssen zuerst diese beiden Bits
	// gesetzt werden:
	WDTCSR |=(1<<WDCE)|(1<<WDE); 
	/* Anschließend werden die eigentlichen Einstellungen vorgenommen */
	WDTCSR=(1<<WDIE)|(0<<WDE)|(0<<WDP3)|(1<<WDP2)|(1<<WDP1)|(0<<WDP0);
	sei();
}
/*****************************************************************************/
void timer1_setup(uint32_t microseconds){
	uint32_t cpu_cycles = (F_CPU/1000000) * microseconds; //!< Anzahl der CPU-Zyklen pro Zeiteinheit

	/* Zurücksetzen/Stoppen des Timers */
	TCCR1A = 0;
	TCCR1B = 0;
	TIMSK1 = 0;
 
	//Siehe Datasheet S. 136
	uint8_t prescale; //!< Prescaler
 
	if (cpu_cycles <= 65536) { //65536 entspricht dem maximalen Wert einer 16 bit Variabeln
		/* Kein Prescaler */
		TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS10);
		prescale = 1;
	}
	else if (cpu_cycles <= 65536*8L) {
		/* Prescaler 8 */
		TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS11);
		prescale = 8;
	}
	else if (cpu_cycles <= 65536*64L) {
		/* Prescaler 64 */
		TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS11) | (1 << CS10);
		prescale = 64;
	}
	else if (cpu_cycles <= 65536*256L) {
		/* Prescaler 256 */
		TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS12);
		prescale = 256;
	}
	else {
		/* Prescaler 1024 */
		TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS12) | (1 << CS10);
		prescale = 1024;
		/* Falls microseconds zu groß gewählt, maximalen Wert verwenden */
		if (cpu_cycles > 65536*1024L) cpu_cycles= 65536*1024L;
	}
	/* Durch Prescaler teilen um tatsächliche Anzahl an Zyklen zu erhalten */
	cpu_cycles= cpu_cycles/prescale;
 
	/* Eigentliche Aktivierung des Timers und setzen der Werte */
	cli();
	/* Setzen des Compare (Vergleichswerts) */
	ICR1 = cpu_cycles - 1;
	/* Counter-Wert auf 0 setzen */
	TCNT1 = 0;
	/* Zurücksetzen wartender Interrupts */
	TIFR1 |= (1 << OCF1B);
	/* Aktivieren des Interrupts */
	TIMSK1 = (1 << OCIE1B);
	sei();

}
/*****************************************************************************/
void EZB_Zeitverwaltung_Init(uint32_t int_time){
#ifdef USE_WDT
	WATCHDOG_SETUP();
// 	WICHTIG! Die folgende Funktion kann nur aufgerufen werden, wenn die
// 	Compilerflag -Os oder andere Optimierungen aktiv sind, ansonsten
// 	wird das System nicht starten.
	//wdt_setup();
#else
	timer1_setup(int_time);
#endif
}

/*****************************************************************************/
/** \brief Default ISR
 *
 * Diese ISR wird immer aufgerufen, falls ein Interrupt aufgerufen wird
 * zu dem keine Funktion hinterlegt wurde.
 *
 * Infomrationen zum Interrupt-Vector können dem Datenblatt entnommen werden.
 */
ISR(BADISR_vect)
{
    while(1){
    	delay(100);
		EZB_Info_Write("ERROR            ");
		EZB_Info_Update();
    }
}
/*****************************************************************************/
#ifdef USE_WDT
/** \brief WDT ISR
 *
 * Diese ISR wird bei jedem Interrupt durch den WDT aufgerufen. Es handelt
 * sich um eine ISR mit dem Attribut "NAKED", also ohne Prolog und Speicherung
 * von Registern. Dies muss daher anschließend manuell übernommen werden.
 *
 * Infomrationen zum Interrupt-Vector können dem Datenblatt entnommen werden.
 */
ISR(WDT_vect, ISR_NAKED){
	SAVE_CONTEXT();
	EZB_Zeit();
	RESTORE_CONTEXT();
	reti();
}
/*****************************************************************************/
#else
/** \brief TIMER1 OUTPUT COMPARE ISR
 *
 * Diese ISR wird bei jedem Interrupt durch den TIMER1 OUTPUT COMPARE aufgerufen.
 * Es handelt sich um eine ISR mit dem Attribut "NAKED", also ohne Prolog und
 * Speicherung von Registern. Dies muss daher anschließend manuell übernommen werden.
 *
 * Informationen zum Interrupt-Vector können dem Datenblatt entnommen werden.
 */
ISR(TIMER1_COMPB_vect, ISR_NAKED){
	SAVE_CONTEXT();
	EZB_Zeit();
	RESTORE_CONTEXT();
	reti();
}

#endif

/*@}*/
/* EOF */