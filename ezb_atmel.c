/**
 * \file ezb_atmel.c
 *
 * \brief Definition der Tasks und Hauptprogramm
 *
 * Diese Datei beinhaltet das eigentliche Hauptprogramm, ebenso wie die Definitionen
 * der einzelnen Tasks.
 *
 */

/* Importierte Dateien */
#include <Arduino.h>
#include "ezb.h"
#include <avr/pgmspace.h>
#include "ezb_led.h"
#include "Makros.h"

/* Strings für die Ausgabe */
prog_char string_taskname_1[]="Aktuatoren  "; //!< Taskname 1
prog_char string_taskname_2[]="Flugregelung"; //!< Taskname 2
prog_char string_taskname_3[]="OS          "; //!< Taskname 3
prog_char string_taskname_4[]="Idle        "; //!< Taskname 4
	
/** \brief Task 1
 *
 * \return None
 */
void Actuators(void){
	while(1){
		EZB_Info_Write_PROGMEM(string_taskname_1);
		delay(200);
		EZB_Wait();
	}
}

/** \brief Task 2
 *
 * \return None
 */
void Flight_control(void){
	while(1){
		EZB_Info_Write_PROGMEM(string_taskname_2);
		delay(200);
		EZB_Wait();
	}
}

/** \brief Task 3
 *
 * \return None
 */
void OS(void){
	while(1){
		EZB_Info_Write_PROGMEM(string_taskname_3);
		delay(200);
		EZB_Wait();
	}
}

/** \brief Task 4
 *
 * \return None
 */
void Idle(void){
	while(1){
		EZB_Info_Write_PROGMEM(string_taskname_4);
		EZB_Wait();
		delay(100);
	}
}

/** \brief Setup Funktion
 *
 *
 * Diese Funktion wird zu Beginn des Programms einmal durchlaufen. Hier werden also alle
 * Initialisierungs Funktionen aufgerufen
 * \return None
 */
void setup() {
	EZB_Init();
	EZB_CreateTask(Idle,1,4);
	EZB_CreateTask(OS,2,3);
	EZB_CreateTask(Flight_control,20,1);
	EZB_CreateTask(Actuators,4,2);
	EZB_SystemStart();
	while(1){
		/* Diese Schleife wird nie laufen */
		delay(100);
}
}

/** \brief Loop Funktion
 *
 *
 * Diese Funktion läuft in einer Endlosschleife ab. Für gewöhnlich enhält sie den hauptsächlichen Code.
 * Hier bleibt sie allerdings leer.
 * \return None
 */
void loop() {
	
	/* Diese Schleife wird nie laufen */
	delay(100);
  
}

/* EOF */