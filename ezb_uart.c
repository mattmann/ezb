/**
 * \file ezb_uart.c
 *
 * \brief Funktionen und Definitionen zur Seriellen Ein- und Ausgabe
 *
 * Diese Datei beinhaltet alle Defitionen und Funktionen die zur Seriellen Ein- und Ausgabe verwendet
 * werden.
 *
 */

/** \addtogroup UART
 * \brief Funktionen und Definitionen zur Seriellen Ein- und Ausgabe
 */
/*@{*/

/* Importiere Dateien */
#include <avr/io.h>
#include "ezb_uart.h"

#define F_CPU 16000000UL //!< CPU Taktung
#define UART_BAUD  115200 //!< Baud Rate

/*****************************************************************************/
void EZB_uart_init(void)
{

	unsigned int ubrr = (unsigned int)(F_CPU / (16UL * UART_BAUD));

	UBRR0H = (unsigned char) (ubrr >>8);
	UBRR0L = (unsigned char) (ubrr);
	/* tx/rx enable */
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	/* 8-bit, 1 stop bit, no parity, asynchronous UART */
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00) | (0 << USBS0) |
	(0 << UPM01) | (0 << UPM00) | (0 << UMSEL01) |
	(0 << UMSEL00);
}
/*****************************************************************************/
int EZB_uart_putchar(char c)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	if (c == '\n')  EZB_uart_putchar('\r');
	UDR0 = c;
	return 0;
}
/*****************************************************************************/
int EZB_uart_getchar(void)
{
	unsigned int reg;
	reg = UCSR0A & (1<<RXC0);
	//loop_until_bit_is_set(UCSR0A, RXC0);
	if ( reg) 	return UDR0;
	else return 0;
}

/*@}*/
/* EOF */