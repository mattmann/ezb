/**
 * \file ezb_out.c
 *
 * \brief Funktionen zur Ausgabe auf die GUI
 *
 * Diese Datei enthält die Funktionen zur Ausgabe auf die GUI
 *
 */

/** \addtogroup GUI
 * \brief Funktionen zur Ausgabe auf die GUI
 */
/*@{*/

/* Importierte Dateien */
#include "ezb_out.h"
#include "ezb.h"
#include "ezb_uart.h"
#include <stdio.h>
#include <avr/pgmspace.h>

/*****************************************************************************/
void write_to( char * aText)
{
	int i = 0;

	while(aText[i] != '\0'){
		EZB_uart_putchar(aText[i]);
		i++;
	}
}
/*****************************************************************************/
void write_from_id(char* string)
{
	uint8_t i=0;
	char buffer;
	while(buffer=(char)pgm_read_byte(&(string[i]))){
		EZB_uart_putchar(buffer);
		i++;
	}
}
/*****************************************************************************/
void writeBottom(char* string)
{
  gotoxy(MAX_WINDOW_HEIGHT,1);
  write_from_id(string);
}
/*****************************************************************************/
void writeTop(char* string)
{
  gotoxy(1,1);
  write_from_id(string);
}

/*****************************************************************************/
void gotoxy(int x, int y)
{
  char str[30];
  sprintf(str,"\x1B[%d;%dH",x,y);
  write_to(str);
}
/*****************************************************************************/
void clrscr(void)
{
	write_to("\x1B[2J"); /* Clear screen */
}
/*****************************************************************************/
void clrrow(int row)
{
	gotoxy(row,0);
	uint8_t i;
	for(i=0; i<MAX_WINDOW_WIDTH; i++){
		write_to(" ");
	}
}
/*****************************************************************************/
void blackscr(void)
{
	uint8_t i;
	for(i=0; i<MAX_WINDOW_HEIGHT; i++){
		clrrow(i);
	}
}
/*****************************************************************************/
void SetColor(int VorderGrund, int HinterGrund)
{
  char str[30];
  sprintf(str,"\x1B[%d;%dm", HinterGrund, VorderGrund);
  write_to(str);
}
/*****************************************************************************/
void Go_Home(void)
{
	write_to("\x1B[H");
}
/*****************************************************************************/
void Send_Alive_Signal(void)
{
  static int i=0;
  gotoxy(MAX_WINDOW_HEIGHT,MAX_WINDOW_WIDTH-1);
  switch(i)
  {
  case 0:
    {
    	write_to("\\");
      i++;
      break;
    }
  case 1:
    {
    	write_to("|") ;
      i++;
      break;
    }
  case 2:
    {
    	write_to("/") ;
      i++;
      break;
    }
  case 3:
    {
    	write_to("-") ;
      i=0;
      break;
    }
  default:
    {
      i=0;
    }
  }
}

/*@}*/
/* EOF */
