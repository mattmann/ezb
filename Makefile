#Dateipfad der Arduino IDE
ARDUINOPATH=/Applications/Arduino 1.5.2beta.app/Contents/Resources/Java
#Dateipfad der Arduino spezifischen Dateien
ARDUINOBIN=$(ARDUINOPATH)/hardware/tools/avr/bin/
#USB-Port
USBTTY=/dev/tty.usbmodem26221
#Baudrate
BAUDRATE=115200

#Hauptdatei
mainfile=ezb_atmel
#Benötigte Header-Dateien
headers=Makros.h ezb_led.h ezb.h ezb_out.h ezb_uart.h
#Weitere Abhängigkeiten
dependencies=ezb_led.o ezb_taskverwaltung.o ezb_prozessorverwaltung.o ezb_zeitverwaltung.o  ezb_info.o ezb_out.o ezb_uart.o

######################################################################
#Verwendeter Compiler
CC="$(ARDUINOBIN)avr-gcc"
#Zum Erstellen von .hex-Dateien
HEX="$(ARDUINOBIN)avr-objcopy"
#Zum Uploaden auf das Board
AVRDUDE="$(ARDUINOBIN)avrdude"
#Konfigurationsdatei für den Upload
AVRDUDEFLAGS=-C "$(ARDUINOPATH)/hardware/tools/avr/etc/avrdude.conf"
#Zum Emulieren des Boards
SIMULAVR=simulavr
GDB="$(ARDUINOBIN)avr-gdb"
#Compiler Flaggen
CFLAGS= -std=c99 -Os -ffunction-sections -fdata-sections -w -I "$(ARDUINOPATH)/hardware/arduino/avr/cores/arduino" -I "/$(ARDUINOPATH)/hardware/arduino/avr/variants/standard" -mmcu=atmega328p  -DF_CPU=16000000UL -DARDUINO=152 -g
LDFLAGS=-lm -Wl,--gc-sections

CXX=$(CC)
AS=$(CC)
CXXFLAGS=$(CFLAGS)
ASFLAGS=$(CFLAGS)

#Mit diesem Befehl wird das Programm hochgeladen und ausgeführt
run: upload
	killall SCREEN || true
	echo "***** Hit Ctrl-A k y to abort..."
	screen $(USBTTY) $(BAUDRATE)
	killall SCREEN || true

#Mit diesem Befehl wird das Programm hochgeladen
upload: $(mainfile).hex
	killall SCREEN || true
	$(AVRDUDE) $(AVRDUDEFLAGS) -F -V -c arduino -p ATMEGA328P -P $(USBTTY) -b $(BAUDRATE) -U flash:w:$^

#Mit diesem Befehl wird das Programm im Simulator ausgeführt
simulate: $(mainfile).elf
	@echo "***** Hit Ctrl-C to abort..."
	$(SIMULAVR) --device atmega328 --file $< -W 0xc6,-

#Mit diesem Befehl kann im Simulator Debugging durchgeführt werden
debug: $(mainfile).elf
	$(SIMULAVR) --device atmega328 --file $< -W 0xc6,- -g &
	@echo "***** Type quit to exit..."
	$(GDB) -iex "file $<" -iex "target remote localhost:1212" -iex "load" -iex "dir ."
	killall $(SIMULAVR)
	
#Hier findet das Kompilieren statt

#Einbinden aller Header-Dateien
$(mainfile).o: $(headers)

#Erstellen einer .hex-Datei
%.hex: %.elf
	$(HEX) -O ihex -R .eeprom $^ $@

#Erstellen einer .elf-Datei unter Verwendung der Arduino-Dateien, sowie aller eingebundenen Dateien
%.elf: %.c core.a $(dependencies)
	$(CC) $(CFLAGS) -S -fverbose-asm -o $<.s $<
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

#Mit diesem Befehl werden alle vorher erstellten Dateien gelöscht
clean:
	rm -f *.elf *.hex *.o
