/**
 * \file ezb_prozessorverwaltung.c
 *
 * \brief Funktionen und Variablen zur Prozessorverwaltung
 *
 * Diese Datei beinhaltet alle Variabeln und Funktionen die direkt mit der
 * Prozessorverwaltung zu tun haben. Dies beinhaltet vor allem die Freigabe des Prozessors.
 *
 */

/** \addtogroup Prozessor
 * \brief Funktionen und Variablen zur Prozessorverwaltung
 */
/*@{*/

/* Importiere Dateien */
#include "ezb.h"
#include "Makros.h" 

/*****************************************************************************/
void EZB_ExitTask(void){
	TVB_use[run].stack_pointer=STACK_SETUP(TVB_use[run].function, TVB_use[run].stack_top, EZB_ExitTask2);
	EZB_Suche();
	NEW_SP;
	RESTORE_CONTEXT();
	reti();
}
/*****************************************************************************/
void EZB_ExitTask2(void){
	TVB_use[run].stack_pointer=STACK_SETUP(TVB_use[run].function, TVB_use[run].stack_top, EZB_ExitTask);
	EZB_Suche();
	NEW_SP;
	RESTORE_CONTEXT();
	reti();
}
/*****************************************************************************/
void EZB_Wait_Internals(void){
	OLD_SP;
	EZB_Info_Update();
	EZB_Info();
	/* Deaktivieren des laufenden Tasks */
	EZB_Deaktivierung(run);
	run=rdytsk[0];
	run_next=rdytsk[0];
	EZB_Info();
	/* Nächsten Task auf Laufend setzen */
	EZB_Laufend(run_next);
	EZB_Info();
	NEW_SP;
}
/*****************************************************************************/
void EZB_Wait(void){
	SAVE_CONTEXT(); 
	EZB_Wait_Internals();
	RESTORE_CONTEXT();
	reti();
}
/*****************************************************************************/
void EZB_StartTask_Internals(void){
	OLD_SP;
	EZB_Suche();
	EZB_Laufend(run_next);
	NEW_SP;
}
/*****************************************************************************/
void EZB_StartTask(void){
	SAVE_CONTEXT();
	EZB_StartTask_Internals(); 
	RESTORE_CONTEXT();
	reti();
}
/*****************************************************************************/
void EZB_SystemStart(void){
	/* Aktivierung des ersten Tasks */
	EZB_Aktivierung(1);
	GET_SP;
	EZB_StartTask();	
} 
/*@}*/
/* EOF */