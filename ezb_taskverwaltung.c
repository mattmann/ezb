/**
 * \file ezb_taskverwaltung.c
 *
 * \brief Funktionen und Variablen zur Taskverwaltung
 *
 * Diese Datei beinhaltet alle Variabeln und Funktionen die direkt mit den Tasks und ihrer
 * Verwaltung zu tun haben. Dies beinhaltet auch deren Statusänderungen.
 *
 */

/** \addtogroup Tasks
 * \brief Funktionen und Variablen zur Taskverwaltung
 */
/*@{*/

/* Importiere Dateien */
#include "ezb.h"
#include <stdio.h>
#include <stdlib.h>

/* Variablen */
volatile uint32_t time_now; //!< Seit dem starten vergangene Zeit (Anzahl Timer-Ticks)
uint8_t tasks_created; //!< Anzahl der schon erzeugten Tasks
volatile uint8_t rdy_count; //!< Anzahl der bereiten Tasks
volatile uint8_t run; //!< Der aktuell laufende Tasks
volatile uint8_t run_next; //!< Der nächste laufende Task
volatile uint8_t rdytsk[TASKS_NUM]; //!< Geordnete Liste der bereiten Tasks
unsigned SPA; //!< Dient als Ziel für Pointer auf ein Struct
unsigned SPB; //!< Dient als Ziel für Pointer auf ein Struct
struct TVB TVB_use1[TASKS_NUM]; //!< Array mit Tasks
struct TVB *TVB_use=TVB_use1; //!< Pointer auf ein Array mit Tasks
struct Stackpointer *CurrentTVB=(struct Stackpointer*)&SPA; //!< Pointer auf den zuletzt gelaufenen Task
struct Stackpointer *NewTVB=(struct Stackpointer*)&SPB; //!< Pointer auf den als nächstes laufenden Task

/*****************************************************************************/
void EZB_Init(void){
	unsigned sp = (SPH << 8) | SPL; //auslesen des aktuellen Stackpointers
	uint8_t i=0;
	while(i<=TASKS_NUM){ //Initialwerte für alles Tasks setzen
		/*
		TVB_use[i].state=DORMANT;
		TVB_use[i].time_p=0;
 		TVB_use[i].time_w=0;
 		TVB_use[i].time_r=0;
 		TVB_use[i].noend=0;
		*/
		TVB_use[i].stack_top=(sp-(i*STACK_SIZE)); //Setzen des Top of Stack
		i++;
	}
	TVB_use[1].noend=1; //Task 1 darf nicht enden. Task 1 muss immer Idle-Task sein.
	EZB_uart_init();
	EZB_Info_Init();
	EZB_Zeitverwaltung_Init(INT_TIME);
}
/*****************************************************************************/
void EZB_Suche(void){
	/* Zähler der bereiten Tasks nullen */
	rdy_count=0;
	uint8_t i;
	for(i=1; i<=tasks_created; i++){
		if(TVB_use[i].state==READY){ 
			/* Bereite Tasks zählen */
			rdy_count++;
			/* Wenn kein Task läuft, starte Liste */
			if(rdytsk[0]==0){
				rdytsk[0]=i;
			}
			/* Wenn Task läuft, setze ans Ende der Liste */
			else{
				rdytsk[rdy_count-1]=i;
			}
			/* Vergleiche aktuellen Task mit allen anderen */
			uint8_t j;
			for(j=0; j<rdy_count; j++){
				if(TVB_use[i].prio<TVB_use[rdytsk[j]].prio){
					/* Umsortieren der Liste */
					uint8_t m;
					for(m=rdy_count-1; m>j; m--){
						rdytsk[m]=rdytsk[m-1];
					}
					rdytsk[j]=i;
					j=rdy_count;
				}
				else if(TVB_use[i].prio==TVB_use[rdytsk[j]].prio&& rdytsk[j]!=i){
					if(TVB_use[rdytsk[j]].slice_reached==1){
						/* Umsortieren der Liste */
						uint8_t m;
						for(m=rdy_count-1; m>j; m--){
							rdytsk[m]=rdytsk[m-1];
						}
						TVB_use[rdytsk[j]].slice_reached=0;
						TVB_use[rdytsk[j]].time_r=0;
						rdytsk[j]=i;
						j=rdy_count;
					}
					else{
						/* Umsortieren der Liste */
						uint8_t m;
						for(m=rdy_count-1; m>j+1; m--){
							rdytsk[m]=rdytsk[m-1];
						}
						rdytsk[j+1]=i;
						j=rdy_count;
					}				
				}
			}
		}
	}	
	run=rdytsk[0];
	run_next=rdytsk[0];
}
/*****************************************************************************/
enum RETURN_CODE EZB_CreateTask(void (*func)(void), uint8_t cycle, uint8_t priority){
	/* Anzahl der erstellen Tasks inkrementieren */
	tasks_created++;
	TVB_use[tasks_created].function=func;
	TVB_use[tasks_created].time_p=cycle;
	TVB_use[tasks_created].time_w=cycle;
	TVB_use[tasks_created].prio=priority;
	TVB_use[tasks_created].stack_pointer=STACK_SETUP(TVB_use[tasks_created].function, TVB_use[tasks_created].stack_top, EZB_ExitTask);
	EZB_Info_Update_Task(tasks_created);
	return NO_ERROR;
}
/*****************************************************************************/
void EZB_Aktivierung(uint8_t i){
	if(TVB_use[i].function){
		TVB_use[i].state=READY;
	}
}
/*****************************************************************************/
void EZB_Deaktivierung(uint8_t i){
	/* Falls die Funktion enden darf */
	if(TVB_use[i].noend!=1){
		TVB_use[i].state=DORMANT;
		TVB_use[i].time_r=0;
		TVB_use[i].slice_reached=0;
		/* Zähler bereiter Funktionen derementieren */
		rdy_count--;
		uint8_t i;
		for(i=0; i<=rdy_count-1; i++){
			rdytsk[i]=rdytsk[i+1];
		}
		rdytsk[rdy_count]=0;			
	}
}
/*****************************************************************************/
void EZB_Laufend(uint8_t i){
	TVB_use[i].state=RUNNING;
}
/*****************************************************************************/
void EZB_Blockiert(uint8_t i){
	TVB_use[i].state=BLOCKED;
}
/*****************************************************************************/
unsigned STACK_SETUP(void (*starting_point)(void), unsigned stack_top, void (*EZB_ExitTask)(void)){
	unsigned new_sp;
	/* Diese Inline-Assembler Funktion baut einen Stack auf um einen ursprünglichen Context-Switch
	 * zu ermöglichen. Die gepushten Register sind hier alle NULL
	 */
	__asm__ volatile("push r16\n\t" \
				 "push r17\n\t" \
				 "in r16, __SP_L__\n\t" \
				 "in r17, __SP_H__\n\t" \
				 "mov r0, %A1\n\t" \
				 "out __SP_L__, r0\n\t" \
				 "mov r0, %B1\n\t" \
				 "out __SP_H__, r0\n\t" \
				 "mov r0, %A2\n\t" \
				 "push r0\n\t" \
				 "mov r0, %B2\n\t" \
				 "push r0\n\t" \
				 "mov r0, %A3\n\t" \
				 "push r0\n\t" \
				 "mov r0, %B3\n\t" \
				 "push r0\n\t" \
				 "clr r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "push r0\n\t" \
				 "in r0, __SP_L__ \n\t"	\
				 "mov %A0, r0 \n\t"	\
				 "in r0, __SP_H__ \n\t"	\
				 "mov %B0, r0 \n\t"	\
				 "out __SP_L__, r16\n\t" \
				 "out __SP_H__, r17\n\t" \
				 "pop r17\n\t" \
				 "pop r16\n\t" \
				 : "=r" (new_sp)\
				 : "r" (stack_top), "w" (EZB_ExitTask), "w" (starting_point) \
				 );
	return new_sp;
}
/*@}*/
/* EOF */