/**
 * \file ezb_led.h
 *
 * \brief Deklaration der Funktionen zur Verwaltung der On-Board LEDs
 *
 * Diese Datei deklariert die Funktionen zur Verwaltung der LEDs als extern.
 *
 */

/** \addtogroup LEDs
 * \brief Deklaration der Funktionen zur Verwaltung der On-Board LEDs
 */
/*@{*/

#ifndef EZB_LED_H_
#define EZB_LED_H_
#ifdef __cplusplus
extern "C"{
#endif

/** \brief Initialisierungsfunktion der LEDs
 *
 * Diese Funktion initialisiert die LEDs über ihren jeweiligen Port.
 * \return None
 */
void EZB_LED_Init(void);

/** \brief Aktivieren einer LED
 *
 * Diese Funktion aktiviert die LED.
 * \return None
 */
void EZB_LED_TurnOn(void);

/** \brief Deaktivieren einer LED
 *
 * Diese Funktion deaktiviert die LED.
 * \return None
 */
void EZB_LED_TurnOff(void);

/** \brief Umschalten einer LED
 *
 * Diese Funktion schaltet die LED zwischen An und Aus um.
 * \return None
 */
void EZB_LED_Toggle(void);

#ifdef __cplusplus
}
#endif
#endif /* EZB_LED_H */
/*@}*/
/* EOF */
