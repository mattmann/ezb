/**
 * \file ezb_uart.h
 *
 * \brief Deklaration der Funktionen zur Seriellen Ein- und Ausgabe
 *
 * Diese Datei deklariert die Funktionen zur Ein- und Ausgabe über die Serielle Schnittstelle.
 */

/** \addtogroup UART
 * \brief Deklaration der Funktionen zur Seriellen Ein- und Ausgabe
 */
/*@{*/

#ifndef UART_H
#define UART_H
#ifdef __cplusplus
extern "C"{
#endif
	
/** \brief Initialisierungsfunktion der USART
 *
 * Diese Funktion initialisiert die USART.
 * \return None
 */
void	EZB_uart_init(void);

/** \brief Ausgabefunktion über USART
 *
 * Diese Funktion gibt ein Zeichen über UART aus und wartet bis das Register wieder frei ist.
 * \param c [out] Auszugebendes Zeichen
 * \return None
 */
int	EZB_uart_putchar(char c);

/** \brief Einlesefunktion über USART
 *
 * Diese Funktion liest ein Zeichen über UART ein
 * \return None
 */
int	EZB_uart_getchar(void);
#ifdef __cplusplus
}
#endif
#endif /* EZB_UART_H */

/*@}*/
/* EOF */