/**
 * \file ezb_out.h
 *
 * \brief Deklaration der Funktionen zur Ausgabe auf die GUI
 *
 * Diese Datei deklariert die Funktionen zur Ausgabe auf die GUI
 *
 */

/** \addtogroup GUI
 * \brief Hier werden alle Funktionen zur Ein- und Ausgabe auf die GUI deklariert.
 * Das beinhaltet unter anderem solche für den eigentlichen Transport von Daten
 * als auch solche zur graphischen Darstellung.
 */
/*@{*/

#ifndef EZB_OUT_H
#define EZB_OUT_H

#define VORDER_GRUND_SCHWARZ    30 //!< Vordergrund Schwarz
#define VORDER_GRUND_ROT        31 //!< Vordergrund Rot
#define VORDER_GRUND_GRUEN      32 //!< Vordergrund Grün
#define VORDER_GRUND_GELB       33 //!< Vordergrund Gelb
#define VORDER_GRUND_BLAU       34 //!< Vordergrund Blau
#define VORDER_GRUND_VIOLETT    35 //!< Vordergrund Violett
#define VORDER_GRUND_KOBALTBLAU 36 //!< Vordergrund Kobaltblau
#define VORDER_GRUND_WEISS      37 //!< Vordergrund Weiß
#define STD_VORDER_GRUND  VORDER_GRUND_WEISS //!< Vordergrundfarbe

#define HINTER_GRUND_SCHWARZ    40 //!< Hintergrund Schwarz
#define HINTER_GRUND_ROT        41 //!< Hintergrund Rot
#define HINTER_GRUND_GRUEN      42 //!< Hintergrund Grün
#define HINTER_GRUND_GELB       43 //!< Hintergrund Gelb
#define HINTER_GRUND_BLAU       44 //!< Hintergrund Blau
#define HINTER_GRUND_VIOLETT    45 //!< Hintergrund Violett
#define HINTER_GRUND_KOBALTBLAU 46 //!< Hintergrund Kobaltblau
#define HINTER_GRUND_WEISS      47 //!< Hintergrund Weiß
#define STD_HINTER_GRUND  HINTER_GRUND_SCHWARZ //!< Hintergrundfarbe

#define MAX_WINDOW_HEIGHT 25 //!< Maximale Fensterhöhe
#define MAX_WINDOW_WIDTH 80 //!< Maximale Fensterbreite

#ifdef __cplusplus
extern "C"{
#endif
	
/** \brief Unterste Zeile beschreiben
 *
 * Diese Funktion schreibt Text in die unterste Zeile der GUI.
 * \param aText [in] String zur Ausgabe
 * \return None
 */
void writeBottom( char * aText);

/** \brief Oberste Zeile beschreiben
 *
 * Diese Funktion schreibt Text in die oberste Zeile der GUI.
 * \param aText [in] String zur Ausgabe
 * \return None
 */
void writeTop( char * aText);

/** \brief Zeile beschreiben
 *
 * Diese Funktion schreibt Text in eine Zeile der GUI.
 * \param aText [in] String zur Ausgabe
 * \return None
 */
void write_to( char * aText);

/** \brief Zeile beschreiben
 *
 * Diese Funktion schreibt einen statischen String in eine Zeile der GUI.
 * \param string [in] String zur Ausgabe
 * \return None
 */
void write_from_id( char *string);

/** \brief Cursorbewegung
 *
 * Diese Funktion bewegt den Cursor an eine bestimmte Position der GUI.
 * \param x [in] Horizontale Position des Cursors
 * \param y [in] Vertikale Position des Cursors
 * \return None
 */
void gotoxy(int x, int y);
	
/** \brief Terminal zurücksetzen
 *
 * Diese Funktion leert das Terminal von allen Ausgaben.
 * \return None
 */
void clrscr(void);

/** \brief Zeile leeren
 *
 * Diese Funktion überschreibt eine komplette Zeile mit Leerzeichen. Dies
 * sorgt dafür, dass auch bei Terminals mit weißem Hintergrund eine einheitliche
 * Hintergrundfarbe gegeben ist.
 * \return None
 * \param row [in] Nummer der zu löschenden Reihe
 */
void clrrow(int row);
 
/** \brief Bildschirm schwärzen
 * 
 * Diese Funktion färbt den gesamten Bildschirm schwarz ein. Das sorgt bei Terminals
 * mit weißem Hintergrund für eine einheitliche Farbe.
 * \return None
 */
void blackscr(void);

/** \brief Cursor zurücksetzen
 *
 * Diese Funktion setzt den Cursor zurück in die linke obere Ecke.
 * \return None
 */
void Go_Home(void);

/** \brief Farbe setzen
 *
 * Diese Funktion setzt die Farben der GUI
 * \param VorderGrund [in] Vordergrund Farbe
 * \param HinterGrund [in] Hintergrund Farbe
 * \return None
 */
void SetColor(int VorderGrund, int HinterGrund);

/** \brief Lebenszeichen senden
 *
 * Diese Funktion sendet ein rotierendes Symbol in der rechten unteren Ecke als Lebenszeichen.
 * \return None
 */
void Send_Alive_Signal(void);

#ifdef __cplusplus
}
#endif
#endif /* EZB_OUT_H */

/*@}*/
/* EOF */