/**
 * \file Makros.h
 *
 * \brief Deklaration aller Makros
 *
 * Diese Datei deklariert alle Makros.
 *
 */

#ifndef Makros_h
#define Makros_h

#define OLD_SP TVB_use[run].stack_pointer=CurrentTVB[0].stack_pointer //!< Makro zum Speichern des alten SP
#define NEW_SP NewTVB[0].stack_pointer=TVB_use[run_next].stack_pointer //!< Makro zum Laden des neuen SP
#define GET_SP CurrentTVB[0].stack_pointer=(SPH << 8) | SPL; //!< Makro um SP abzufragen

/** \brief Makro zum Speichern des Contexts
 *
 * In diesem Makro wird der eigentliche Context-Switch vorbereitet.
 * Zunächst wird der Context des aktuellen Tasks im korrekten
 * Stack-Bereich gesichert. Hierzu wird zunächst r0, dann das Status-Register
 * und anschließend alle anderen Register auf den Stackgeschoben. Am Ende wird
 * dann noch der neue Stackpointer in eine Variable geschrieben
 */
#define SAVE_CONTEXT()  			\
	__asm__ volatile ("push	r0  \n\t"	\
				  "in	r0, __SREG__ \n\t"	\
				  "cli	  \n\t"	\
				  "push	r0  \n\t"	\
				  "push	r1  \n\t"	\
				  "clr	r1  \n\t"	\
				  "push	r2  \n\t"	\
				  "push	r3  \n\t"	\
				  "push	r4  \n\t"	\
				  "push	r5  \n\t"	\
				  "push	r6  \n\t"	\
				  "push	r7  \n\t"	\
				  "push	r8  \n\t"	\
				  "push	r9  \n\t"	\
				  "push	r10  \n\t"	\
				  "push	r11  \n\t"	\
				  "push	r12  \n\t"	\
				  "push	r13  \n\t"	\
				  "push	r14  \n\t"	\
				  "push	r15  \n\t"	\
				  "push	r16  \n\t"	\
				  "push	r17  \n\t"	\
				  "push	r18  \n\t"	\
				  "push	r19  \n\t"	\
				  "push	r20  \n\t"	\
				  "push	r21  \n\t"	\
				  "push	r22  \n\t"	\
				  "push	r23  \n\t"	\
				  "push	r24  \n\t"	\
				  "push	r25  \n\t"	\
				  "push	r26  \n\t"	\
				  "push	r27  \n\t"	\
				  "push	r28  \n\t"	\
				  "push	r29  \n\t"	\
				  "push	r30  \n\t"	\
				  "push	r31  \n\t"	\
				  "lds	r26, CurrentTVB \n\t"	\
				  "lds	r27, CurrentTVB + 1 \n\t"	\
				  "in	r0, __SP_L__ \n\t"	\
				  "st	x+, r0 \n\t"	\
				  "in	r0, __SP_H__ \n\t"	\
				  "st	x+, r0 \n\t"	\
				:: );

/** \brief Makro zum Speichern des Contexts
 *
 * In diesem Makro wird der Context des nächsten Tasks aus dem korrekten
 * Stack-Bereich ausgelesen und restored. Die Variable NewTVB enthält den
 * besagten Stackpointer. Dieser wird eingelesen und an dieser Stelle
 * werden analog zum SAVE_CONTEXT() alle Register vom Stack geholt
 */
#define RESTORE_CONTEXT()  		\
	__asm__ volatile ("lds	r26, NewTVB \n\t"	\
				  "lds	r27, NewTVB + 1 \n\t"	\
				  "ld	r28, x+	 \n\t"	\
				  "out	__SP_L__, r28 \n\t"	\
				  "ld	r29, x+ \n\t"	\
				  "out	__SP_H__, r29 \n\t"	\
				  "pop	r31  \n\t"	\
				  "pop	r30  \n\t"	\
				  "pop	r29  \n\t"	\
				  "pop	r28  \n\t"	\
				  "pop	r27  \n\t"	\
				  "pop	r26  \n\t"	\
				  "pop	r25  \n\t"	\
				  "pop	r24  \n\t"	\
				  "pop	r23  \n\t"	\
				  "pop	r22  \n\t"	\
				  "pop	r21  \n\t"	\
				  "pop	r20  \n\t"	\
				  "pop	r19  \n\t"	\
				  "pop	r18  \n\t"	\
				  "pop	r17  \n\t"	\
				  "pop	r16  \n\t"	\
				  "pop	r15  \n\t"	\
				  "pop	r14  \n\t"	\
				  "pop	r13  \n\t"	\
				  "pop	r12  \n\t"	\
				  "pop	r11  \n\t"	\
				  "pop	r10  \n\t"	\
				  "pop	r9  \n\t"	\
				  "pop	r8  \n\t"	\
				  "pop	r7  \n\t"	\
				  "pop	r6  \n\t"	\
				  "pop	r5  \n\t"	\
				  "pop	r4  \n\t"	\
				  "pop	r3  \n\t"	\
				  "pop	r2  \n\t"	\
				  "pop	r1  \n\t"	\
				  "pop	r0  \n\t"	\
				  "out	__SREG__, r0 \n\t"	\
				  "pop	r0  \n\t"	\
				  :: );
				  
/** \brief Makro zum Aktivieren des WDT
 *
 * In diesem Makro wird der Watchdog-Timer initialisiert und konfiguriert
 * Das Programmieren in Inline Assembler ermöglicht die Einhaltung von 
 * 4 Takten zum setzen der korrekten Register. Die Konfiguration auf
 * unterschiedliche Taktzeiten kann der Datei ezb_zeitverwaltung.c
 * entnommen werden.
 */
#define WATCHDOG_SETUP()		\
	__asm__ volatile("cli  \n\t"	\
				 "wdr  \n\t"	\
				 "lds r16, 0x60  \n\t"	\
				 "ori r16, 0b00011000  \n\t"	\
				 "sts 0x60, r16  \n\t"	\
				 "ldi r16, 0b01000110  \n\t"	\
				 "sts 0x60, r16  \n\t"	\
				 "sei  \n\t"	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 ""	\
				 :: );
               
#endif /* MAKROS_H */

/* EOF */
